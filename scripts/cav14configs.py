CONFIGS = {
  "baseline":    ['--race-instrumenter=standard',        '--vcgen-opt=/noRemovePrivateArrayAccesses', '--vcgen-opt=/noOptimiseBarrierIntervals', '--vcgen-opt=/noEliminateRedundantReadInstrumentation'],
  "rr":          ['--race-instrumenter=standard',        '--vcgen-opt=/noRemovePrivateArrayAccesses', '--vcgen-opt=/noOptimiseBarrierIntervals'                                                        ],
  "rr+bi":       ['--race-instrumenter=standard',        '--vcgen-opt=/noRemovePrivateArrayAccesses'                                                                                                   ],
  "rr+bi+pa":    ['--race-instrumenter=standard'                                                                                                                                                       ],
  "rr+bi+pa_wd": ['--race-instrumenter=watchdog-single'                                                                                                                                                ],
  "noua":        ['--race-instrumenter=standard',        '--vcgen-opt=/noRemovePrivateArrayAccesses', '--vcgen-opt=/noOptimiseBarrierIntervals', '--vcgen-opt=/noEliminateRedundantReadInstrumentation', '--no-uniformity-analysis'],
  "noam":        ['--race-instrumenter=standard',        '--vcgen-opt=/noRemovePrivateArrayAccesses', '--vcgen-opt=/noOptimiseBarrierIntervals', '--vcgen-opt=/noEliminateRedundantReadInstrumentation', '--bugle-opt=-model-bv-as-byte-array'],
}
