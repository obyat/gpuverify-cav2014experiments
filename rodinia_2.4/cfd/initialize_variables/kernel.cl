//pass
//--local_size=[192,1] --num_groups=[506,1]

#include "../common.h"

__kernel void initialize_variables(__global float* variables, __constant float* ff_variable, int nelr){
	//const int i = (blockDim.x*blockIdx.x + threadIdx.x);
	const int i = get_global_id(0);
	for(int j = 0; j < NVAR; j++)
		variables[i + j*nelr] = ff_variable[j];
	
}
