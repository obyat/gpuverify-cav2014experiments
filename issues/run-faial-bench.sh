#script to run files located in unique-files-in-alltxt
#FOUND ALL UNIQUES between all-cuda.txt and inv.txt with 
#grep -Fvxf inv.txt all.txt > new.txt
#./check-drf-run.sh &> check-drf-output.txt
declare -i y=0

#change depending on location of all.txt/inv.txt/all-cuda.txt
cd ..
input="../gpuverify-cav2014experiments/issues/unique-cu.txt"

while IFS= read -r line
  do

    echo -e "\n\n"
    y=$((y + 1))
    echo -e "----------------------------------------------------------------------"
    echo "$y $line"
    echo -e "----------------------------------------------------------------------"
    echo -e "\n"
    faial-bench "$line" 
    echo -e "\n\n"

done < "$input"


